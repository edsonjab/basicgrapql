# GrapQL
## Query
* En un crud un query nos permite leer los registros de la forma de exteraer la informacion existente desde la base de datos o rest api, equivale a un select de SQL o un GET de una RESTAPI.
* En el query declaras que campos o datos vas a requerir para tu consulta y tambien soporta parametros [input].
* El query en GraphQL es universal, por lo tanto es igual en angular, node o react o si la base de datos es NoSQL o SQL, siempre es la misma sintaxis

### Ejemplo
```json
query {
    getProduct {
        id
        name
        price
        stock
    }
}
```

## Mutation
* Se utilizan para las otras 3 entradas del CRUD Actualizar, Eliminar y Crear Registros.
* Similares a un PUT/PATCH, DELETE o POST de una REST API o un DELETE, UPDATE e INSERT en SQL.
* Igual que el Query son Independientes del lenguaje, asi que son iguales si tu backend es Node, PHP o Python o si tu Base de Datos es NoSQL o SQL.
### Ejemplo
```json
mutation deleteProduct($id : ID){
    deleteProduct(id : ID)
}
{
    "data": {
        "deleteProduct": "Se eliminó correctamente"
    }
}
```

## Squema
* Es lo que destribe tus tipos de objeto, queries y datos de tu aplicación.
* Query es el único que es obligatorio en tu schema.
* El Schema en GraphQL utiliza un typing en el que le defines si un campo será de tipo String, Int, Boolean u otro tipo de dato.
* El Schema y el Resolver están muy relacionados, el schema define la forma de los datos mientras que el Resolver se encarga de la comunicación con el lenguaje del servidor y la Base de Datos.
### Ejemplo
```json
type Customer {
    id: ID
    name: String
    last_name: String
    enterprise: String
    emails: [Email]
    age : Int
}
type Email {
    email: String
}
```
** NOTA: La estructura debe ser similar a la Base de Datos.
## Rersolvers
* Son funciones que son responsables de retornar los valores que existen en tu Schema.
* Queries y mutations por si solos no tienen mucha funcionalidad, requieren un backend para realizar las operaciones en la Base de Datos (son los resolvers).
* Los nombres de los resolvers deben ser iguales a los definidos en el Schema.
### Ejemplo
```javascript
getCustomers: async () => {
    const customers = await Customers.find({});
    return customers;
}
```
## Schema y Resolvers
```javascript
    // En el Schema de GraphQL
type Query {
    getCustomer(id: ID): Customer   --------------->  //type Customer {
}                                                       //id: ID
                                                        //name: String
                                                        //last_name: String
    // En el Resolver:                                    enterprise: String
getCustomer: async (_, { id }) => {                     //emails: [Email]
    // Get Customers                                      age : Int }
    const customer = await Customer.findById( id );
    return customer;
}
```